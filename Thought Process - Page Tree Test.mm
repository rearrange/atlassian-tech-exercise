<map version="1.0.1">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node CREATED="1458293738767" ID="ID_868404747" MODIFIED="1458293754558" TEXT="Page Tree">
<node CREATED="1458293761018" ID="ID_1513007034" MODIFIED="1458293788773" POSITION="right" TEXT="Functional test">
<node CREATED="1458293800010" ID="ID_101546188" MODIFIED="1458300835663" TEXT="How does the page being sorted? ">
<node CREATED="1458294301474" ID="ID_676566143" MODIFIED="1458301439952" TEXT="By title - Alphabetically? ">
<node CREATED="1458298798609" ID="ID_858903841" MODIFIED="1458298816886" TEXT="Result: Page created with purely strings will be sorted based from A-Z">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1458294317199" ID="ID_120133612" MODIFIED="1458294378636" TEXT="How about if page title have: ">
<node CREATED="1458294382501" ID="ID_505839965" MODIFIED="1458294384912" TEXT="Numbers?">
<node CREATED="1458298750393" ID="ID_1974959422" MODIFIED="1458298820871" TEXT="Result: Page created with numbers will go second (after special characters, before date or strings)">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1458294385415" ID="ID_827580355" MODIFIED="1458294390203" TEXT="Special characters?">
<node CREATED="1458295432359" ID="ID_180196183" MODIFIED="1458295462603" TEXT="Test: Create page with special characters">
<node CREATED="1458295465901" ID="ID_1486773959" MODIFIED="1458298824088" TEXT="Result: Page can be created with @, #, ~">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1458298736443" ID="ID_977744295" MODIFIED="1458298826783" TEXT="Result: Special characters page will go first">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1458296074951" ID="ID_1133922052" MODIFIED="1458296086366" TEXT="Extended test: Create page with special characters from BLNS">
<node CREATED="1458296087847" ID="ID_162802354" MODIFIED="1458298570382" TEXT="Emoticons">
<node CREATED="1458298571781" ID="ID_712144153" MODIFIED="1458298576459" TEXT="Japanese Emoticons"/>
<node CREATED="1458298576814" ID="ID_335756625" MODIFIED="1458298676448" TEXT="Double-byte Emoji"/>
</node>
<node CREATED="1458298626031" ID="ID_601723747" MODIFIED="1458298631758" TEXT="I18N / L11N">
<node CREATED="1458298632868" ID="ID_634018893" MODIFIED="1458298642481" TEXT="RTL language (arabic)"/>
<node CREATED="1458298642931" ID="ID_1404543295" MODIFIED="1458298697789" TEXT="Japanese/Korean/Chinese characters"/>
<node CREATED="1458298689036" ID="ID_1766636436" MODIFIED="1458300687123" TEXT="Russian/French/"/>
</node>
<node CREATED="1458296102393" ID="ID_1685541464" MODIFIED="1458296149079" TEXT="NULL representation">
<node CREATED="1458296151130" ID="ID_1640021349" MODIFIED="1458298835641" TEXT="NULL">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1458296161245" ID="ID_959817235" MODIFIED="1458296163365" TEXT="(null)"/>
<node CREATED="1458296163906" ID="ID_1559661500" MODIFIED="1458296170120" TEXT="nil"/>
<node CREATED="1458296207211" ID="ID_1722399519" MODIFIED="1458296208707" TEXT="NIL"/>
<node CREATED="1458393106375" ID="ID_1854880047" MODIFIED="1458393193236" TEXT="&quot; &quot; (blank space)">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1458300707328" ID="ID_965768946" MODIFIED="1458300712325" TEXT="Unicode representation">
<node CREATED="1458300723352" ID="ID_11627954" MODIFIED="1458300724560" TEXT="&#x3a9;&#x2248;&#xe7;&#x221a;&#x222b;&#x2dc;&#xb5;&#x2264;&#x2265;&#xf7;"/>
</node>
</node>
</node>
<node CREATED="1458298451737" ID="ID_1704156293" MODIFIED="1458298485516" TEXT="Date Format?">
<node CREATED="1458298465715" ID="ID_328748608" MODIFIED="1458298526429" TEXT="YYYY/MM/DD">
<icon BUILTIN="button_ok"/>
<node CREATED="1458298861624" ID="ID_1874005733" MODIFIED="1458298903406" TEXT="Result: Looks like it is treated as number?">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1458298494527" ID="ID_1106333290" MODIFIED="1458298500580" TEXT="DD/MM/YYYY"/>
</node>
</node>
<node CREATED="1458301450867" ID="ID_1906866249" MODIFIED="1458301462066" TEXT="Does  the page content affects Page Tree? ">
<node CREATED="1458301463669" ID="ID_256453069" MODIFIED="1458301502441" TEXT="Test: Create page with image as the content">
<icon BUILTIN="button_ok"/>
<node CREATED="1458301486994" ID="ID_527180569" MODIFIED="1458301505033" TEXT="Result: No, Page Tree is not affected by the content">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1458294509606" ID="ID_79306027" MODIFIED="1458294531016" TEXT="Test: &quot;Default&quot; sorting offered by Page Tree">
<node CREATED="1458294914078" ID="ID_940369278" MODIFIED="1458298848739" TEXT="Result: special characters, numbers, date, strings">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
<node CREATED="1458294788903" ID="ID_229035610" MODIFIED="1458394611496" TEXT="Level of pages that can be shown in Page Tree">
<node CREATED="1458294804499" ID="ID_17478542" MODIFIED="1458298914004" TEXT="Single page, no child. ">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1458295088571" ID="ID_993062773" MODIFIED="1458300815926" TEXT="Parent page, one child.">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1458298925420" ID="ID_75742388" MODIFIED="1458300817879" TEXT="Parent page, multiple child.">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1458294808131" ID="ID_657156041" MODIFIED="1458300821806" TEXT="Child Page, no child below.">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1458294863908" ID="ID_1842242735" MODIFIED="1458300825182" TEXT="Child page, with another child(s) below. ">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1458294873484" ID="ID_590683913" MODIFIED="1458294892189" TEXT="Boundary Test: How many level of pages can be shown by Page Tree?">
<node CREATED="1458294893919" ID="ID_1495190227" MODIFIED="1458298944279" TEXT="Should be more than 10 at least">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1458297424229" ID="ID_1265149109" MODIFIED="1458395763627" TEXT="Extended test: 10 parents, 10 childs? = 100 pages"/>
</node>
</node>
<node CREATED="1458295541441" ID="ID_83481371" MODIFIED="1458395767427" TEXT="Drag and drop the Page list ">
<node CREATED="1458295561207" ID="ID_1974857960" MODIFIED="1458300505653" TEXT="Test: Can user drag the first page to the bottom (last) position?">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1458295586026" ID="ID_135029722" MODIFIED="1458300508590" TEXT="Test: Can user drag the bottom page to the first position?">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1458300894461" ID="ID_379818229" MODIFIED="1458300906818" TEXT="Test: Click, hold, and drag around the screen">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1458293904324" ID="ID_1084027363" MODIFIED="1458398032231" TEXT="What are other functionality offered by Page Tree?">
<node CREATED="1458295207511" ID="ID_1505481612" MODIFIED="1458295272257" TEXT="FOUND: if there are more than 1 child page, the AZ icon appears">
<node CREATED="1458295278466" ID="ID_1333549277" MODIFIED="1458298960426" TEXT="Icon functionality: sort based on &quot;Default&quot;">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
<node CREATED="1458293771501" ID="ID_1261295269" MODIFIED="1458293781318" POSITION="left" TEXT="Non-functional test">
<node CREATED="1458294412563" ID="ID_1397719893" MODIFIED="1458294424314" TEXT="Performance test --- would it be possible?">
<node CREATED="1458294426020" ID="ID_1216981113" MODIFIED="1458294437974" TEXT="Maybe not under 30 mins :("/>
</node>
<node CREATED="1458297835468" ID="ID_1233646891" MODIFIED="1458297839316" TEXT="Interactions">
<node CREATED="1458293931309" ID="ID_705370446" MODIFIED="1458293973506" TEXT="Can users use mouse to interact with the Page Tree?">
<node CREATED="1458293983667" ID="ID_1510378713" MODIFIED="1458293990089" TEXT="Yes">
<node CREATED="1458293992106" ID="ID_407793077" MODIFIED="1458300565101" TEXT="Test: Can user drag multiple level of trees up and down?">
<icon BUILTIN="button_ok"/>
<node CREATED="1458295990091" ID="ID_1142382990" MODIFIED="1458301209202" TEXT="Result: Moving down trees cause bug in Firefox">
<icon BUILTIN="button_ok"/>
</node>
</node>
</node>
</node>
<node CREATED="1458293947161" ID="ID_922426434" MODIFIED="1458293962538" TEXT="Can users use keyboard to interact with the Page Tree?">
<node CREATED="1458295365813" ID="ID_1801962474" MODIFIED="1458295376084" TEXT="No. No signs of shortcut available for Page Tree"/>
</node>
</node>
<node CREATED="1458298140908" ID="ID_1907009427" MODIFIED="1458298142394" TEXT="Labels">
<node CREATED="1458298143733" ID="ID_448993533" MODIFIED="1458298148515" TEXT="&quot;View in Hierarchy&quot; ">
<node CREATED="1458298157383" ID="ID_1906493598" MODIFIED="1458298211745" TEXT="Not ambiguous --- users can understand that its a &quot;tree&quot;"/>
</node>
<node CREATED="1458301252439" ID="ID_793907619" MODIFIED="1458301257748" TEXT="&quot;Reorder Page&quot; tab">
<node CREATED="1458301259581" ID="ID_1348400955" MODIFIED="1458301265194" TEXT="Clear enough for user to understand"/>
</node>
</node>
<node CREATED="1458298292985" ID="ID_902092314" MODIFIED="1458298297975" TEXT="Browser compatibilty">
<node CREATED="1458298301761" ID="ID_44496505" MODIFIED="1458300521422" TEXT="Test: Use Firefox to interact with Page Tree">
<icon BUILTIN="button_ok"/>
</node>
<node CREATED="1458298326546" ID="ID_1883297045" MODIFIED="1458298344633" TEXT="Test: Use Chrome to interact with Page Tree"/>
<node CREATED="1458298355045" ID="ID_1537293515" MODIFIED="1458298364990" TEXT="Test: Use IE to interact with Page Tree"/>
</node>
<node CREATED="1458300954906" ID="ID_1201035979" MODIFIED="1458301713033" TEXT="Hack the pages - load something else (files, SQL)">
<node CREATED="1458300973624" ID="ID_1730790065" MODIFIED="1458301070224" TEXT="Test: Drag and drop .exe files from desktop to the Page Tree webpage">
<icon BUILTIN="button_ok"/>
<node CREATED="1458301077040" ID="ID_616478548" MODIFIED="1458301124346" TEXT="Result: Browsers will try to execute the .exe files -- warning popup comes out.">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1458300985905" ID="ID_690280807" MODIFIED="1458301071896" TEXT="Test: Drag and drop .pdf files from desktop to the Page Tree webpage.">
<icon BUILTIN="button_ok"/>
<node CREATED="1458301105633" ID="ID_825809301" MODIFIED="1458301122458" TEXT="Result: Browsers will render the PDF and act like a PDF viewer">
<icon BUILTIN="button_ok"/>
</node>
</node>
<node CREATED="1458301369921" ID="ID_588470642" MODIFIED="1458301382813" TEXT="Can users put pictures into the tree structure? ">
<node CREATED="1458301385645" ID="ID_1455946545" MODIFIED="1458301400263" TEXT="Test: Drag and drop image files from desktop to the Page Tree webpage">
<node CREATED="1458301401657" ID="ID_1307636624" MODIFIED="1458301410295" TEXT="Image format: JPEG"/>
<node CREATED="1458301411253" ID="ID_1564329299" MODIFIED="1458301415223" TEXT="Image format: PNG"/>
<node CREATED="1458301415726" ID="ID_1307299029" MODIFIED="1458301420367" TEXT="Image format: GIF"/>
</node>
</node>
<node CREATED="1458301289589" ID="ID_1891916921" MODIFIED="1458301563325" TEXT="Extended test: Create page with XSS javascript">
<node CREATED="1458301518664" ID="ID_34568989" MODIFIED="1458301524770" TEXT="Should have been part of BLNS test"/>
</node>
<node CREATED="1458301564532" ID="ID_1607136395" MODIFIED="1458301583968" TEXT="Extended test: SQL injection">
<node CREATED="1458301585747" ID="ID_1432699184" MODIFIED="1458301614223" TEXT="Test: Create page with &quot;1;DROP TABLE users&quot;"/>
<node CREATED="1458301617044" HGAP="21" ID="ID_1946124294" MODIFIED="1458301631472" TEXT="Test: Create page with &quot;1&apos;; DROP TABLE users-- 1&quot;" VSHIFT="1"/>
<node CREATED="1458301633094" ID="ID_1680783007" MODIFIED="1458301667946" TEXT="Test: Create page with &quot;&apos; OR 1=1 -- 1&quot;"/>
<node CREATED="1458301654346" ID="ID_66633812" MODIFIED="1458301660042" TEXT="Test: Create page with &quot;&apos; OR &apos;1&apos;=&apos;1&quot;"/>
</node>
</node>
</node>
</node>
</map>
