/*
 * The MIT License
 *
 * Copyright 2016 rearrange.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.bitbucket.rearrange.confluence.automation.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author rearrange
 */
public class HomePage {
    private static final String CONFLUENCE_HOME = 
            "https://rearrange.atlassian.net/wiki";
    private final WebDriver driver;
    
    @FindBy(id="quick-create-page-button") WebElement CREATE_BUTTON;
       
    public HomePage (WebDriver driver) {
        this.driver = driver;
    }
    
    public HomePage open() {
        driver.get(CONFLUENCE_HOME);
        return PageFactory.initElements(driver, HomePage.class);        
    }
  
    public CreatePageAction clickCreateButton() {
        CREATE_BUTTON.click();
        return PageFactory.initElements(driver, CreatePageAction.class);
    }
    
}
