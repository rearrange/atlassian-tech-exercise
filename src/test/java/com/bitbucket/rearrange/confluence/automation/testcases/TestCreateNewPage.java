/*
 * The MIT License
 *
 * Copyright 2016 rearrange.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.bitbucket.rearrange.confluence.automation.testcases;

import com.bitbucket.rearrange.confluence.automation.pages.SpacePage;
import com.bitbucket.rearrange.confluence.automation.pages.CreatePageAction;
import com.bitbucket.rearrange.confluence.automation.pages.HomePage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 *
 * @author rearrange
 */
public class TestCreateNewPage extends BaseTestSuite {
    
    @Test
    public void testCreateNewPageUsingTitle() {
        
        CreatePageAction createPageAction;
        SpacePage goToSpacePage;
        String newPageTitle ="Create a Jekyll Blog for marketing";
        
        homePage = PageFactory.initElements(driver, HomePage.class);        
        
        /* Test step: 
        ** 1) Click Create button
        ** 2) Enter title for the new page
        ** 3) Click Save button
        */
        createPageAction = homePage.clickCreateButton();
        createPageAction.enterTitle(newPageTitle);
        createPageAction.clickSaveButton();
        
        /* Verification step:
        ** 1) Go to Space page
        ** 2) Check link exist or not.
        */
        goToSpacePage = PageFactory.initElements(driver, SpacePage.class);
        goToSpacePage.findLink(newPageTitle);
        assert(goToSpacePage.findLink(newPageTitle).contains(newPageTitle));
    }
    
}
