/*
 * The MIT License
 *
 * Copyright 2016 rearrange.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.bitbucket.rearrange.confluence.automation.testcases;

import com.bitbucket.rearrange.confluence.automation.pages.ExistingPage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 *
 * @author rearrange
 */
public class TestSetRestriction extends BaseTestSuite {
    
    @Test
    public void testSetPageViewingRestricted() {
        ExistingPage existingPage, reloadPage;
        
        existingPage = PageFactory.initElements(driver, ExistingPage.class);
        existingPage.open();
        existingPage.clickRestrictIcon();
        existingPage.selectViewEditRestrictOption();
        existingPage.clickSaveButtonAfterRestrict();
        
        /*
        ** Verification steps:
        ** 1) Reload page
        ** 2) Click restrict icon.
        ** 2) Ensure that the restrict selected is "Viewing and editing restricted"
        */

        reloadPage = PageFactory.initElements(driver, ExistingPage.class);
        reloadPage.open();
        reloadPage.clickRestrictIcon();
        String optionSelected = reloadPage.restrictOptionSelected();
        assert(optionSelected).equals("Viewing and editing restricted");
    }
    
}
