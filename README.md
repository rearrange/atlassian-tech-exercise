# README #

Atlassian Technical Exercise for QA Hiring - done by Sallehin Sallehuddin.

## What is this repository for? ##

* Quick summary: This is a repository containing answers for Atlassian Technical Exercise for QA hiring, done by me. 
* The first exercise is to write two automated browser-based test for Confluence cloud: Create a new page in Confluence, and set restrictions at existing page in Confluence.
* The second exercise is to do exploratory test on Page Tree feature in Confluence cloud. 
* Version: 1.0

## PART 1 - Automated Test ##
### Dependencies and Assumptions ###

* Application Under Test (AUT) is a 7-day trial version of [Confluence Cloud](https://rearrange.atlassian.net) 
* AUT has an existing default space called "Ideas in My Head" which can be accessed via [/display/ideas](https://rearrange.atlassian.net/wiki/display/ideas)
* AUT has an existing page under the space above called "[Personal Finance Software](https://rearrange.atlassian.net/wiki/display/ideas/Personal+Finance+Software)" 
* The test will create a new page with title "Create a Jekyll Blog for marketing". Ensure that there's no page with the same title before running the test.  
* The test will set restriction on "[Personal Finance Software](https://rearrange.atlassian.net/wiki/display/ideas/Personal+Finance+Software)" page to viewing and editing restricted. 
* The test has been written under Netbeans as a Maven application, on Windows 7 platform, using Java 1.8 and Selenium WebDriver version 2.52.0.
* The test has been developed following Page Object design pattern with the support of Page Factory capabilities provided by Selenium WebDriver to ease controlling the page workflow. 
* Ensure that Maven can be executed from your path, and Mozilla Firefox browser is updated (*tested with version 44.0.2*) and configured without any proxy. 
* At times the WebElements might not be detected (especially when the popup dialog is more than the loaded Firefox), so a little intervention to maximize the Firefox might be needed for second test. This should be an enhancement to the code --- might need to recode this part better in detecting the WebElement (further read on switch focus needed).  

### How do I get set up? ###

* Clone this repository: git clone https://rearrange@bitbucket.org/rearrange/atlassian-tech-exercise.git 
* Go to the cloned folder: cd /path/to/atlassian-tech-exercise
* Run test: mvn clean test
* See the test running automatically!

## PART 2 - Exploratory Test ##
Refer to the [report](https://bitbucket.org/rearrange/atlassian-tech-exercise/raw/1492045dfef83a17988aaa7cf6a7e3d82adf2f94/Exploratory%20Test%20Report%20-%20Page%20Tree.pdf) provided in the repository for the exploratory test conducted on Page Tree feature.